See also blog post:

[http://michaelb.org/unit-tests-for-your-godot-scripts/](http://michaelb.org/unit-tests-for-your-godot-scripts/)

----

# unittest.gd

`unittest.gd` is a quick-and-dirty unit-testing system for GDScripts, for [the
MIT licensed Godot Game Engine](https://godotengine.org/)


## Installing unittest.gd

#### 1. Download

Download or clone [unittest.gd](https://bitbucket.org/michaelb/unittest.gd/raw/master/unittest.gd) to some spot in your Godot project, such as `res://scripts/`.

Linux one-liner:
`wget https://bitbucket.org/michaelb/unittest.gd/raw/master/unittest.gd`

#### 2. Create a test

Now you gotta write your actual tests. Create a GDScript file that extends
`unittest.gd`, and has a `func test()` method that has a bunch of assertions.
Here are some examples from a tanagrams game:

```javascript
extends "res://scripts/unittest.gd"

func tests():
    testcase("board can place pieces and check for collisions")
    var board = Board.new()
    board.set_dimensions(13, 13)
    board.add_block(0, 0) # adds large block
    assert_false(board.check_placement(0, 0), 'near top left corner')
    assert_true(board.check_placement(2, 0), 'top left corner')
    endcase()

    testcase("rotating a piece 4x results in the original piece for all pieces")
    for i in range(10):
        var original_piece = board.piece_constants.PIECES[i]
        var piece = board.rotate_piece(original_piece, 4)
        assert_dict_equal(piece, original_piece, '4x rotation piece ' + str(i))
    endcase()
```

#### 3. Create a script to run your tests

Now, you'll want to be able to run all your tests at once. `unittest.gd` has a helper to do this. Create a script like the following, add the unit test you just wrote to the list, name it `runtests.gd` (or whatever you want) and plop it into your scripts directory:

```javascript
extends SceneTree
func _init():
    load('res://scripts/unittest.gd').run([
        'res://scripts/example_unit_test.gd',
    ])
    quit()
```

To run all your tests, run the following command in a terminal (where `godot` is your Godot binary, wherever you have it):

```
$ godot -s ./scripts/runtests.gd
- [OK] 1/1 - example_unit_test
- [SUCCESS] 1/1
```

That's it! Happy testing :)

---

# GUT, a possibly better option

The other I know of [GUT](https://github.com/bitwes/Gut) which seems to be
pretty feature filled.

So, use GUT if you want something very full-featured. If you just want a simple
layout and assertions with minimal set-up (a single file), then you might like
this.
