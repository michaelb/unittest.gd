extends SceneTree

func _init():
    load('res://unittest.gd').run([
        # Include a list of tests here, either like:
        # 'res://scripts/tests/some_test.gd'
        # Or like:
        # 'scripts.tests.some_test'
        'res://example_tests.gd',
    ])
    quit()

